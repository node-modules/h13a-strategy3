const packageJson = require("./package.json");
const packageJsonMain = require("../../package.json");
const widgetNamespace = packageJsonMain.name.toLowerCase().replace(/-/g,'');
const widgetName = packageJson.name.toLowerCase().replace(/-/g,'');
const widgetFullName = `${widgetNamespace}.${widgetName}`;

const poke = require("h13a-poke");




$.widget(widgetFullName, {

  options: {
    channel: null,
  },

  _mergeOptions: function(event, data) {
    this.option($.extend(true, this.option(), data));
  },



  _pokeOptions: function(event, incoming) {

    // by using the actual object, we can determine more about the data structure.
    let obj = this.option();
    obj = poke(obj, incoming.data);
    this.option( obj );

  },


  _create: function() {

    // Prepare Destructing
    this.destructorArray = [];

    // Check Configuration Here (optional)
    // noop

    // Prepare Dom
    this.consoleNode = $(`<div/>`);
    this.contentNode = $(`<div/>`);
    this.element.append(this.consoleNode);
    this.element.append(this.contentNode);


    if( this.option('channel') ){

      let channel = this.option('channel').toLowerCase().replace(/[^a-z0-9]/g,'');

      // Establish Listeners -- noisepoke
      let serviceMap = {
        [channel+'merge']:'_mergeOptions',
        [channel+'poke']:'_pokeOptions',

      };

      this._on(document, serviceMap);
      if( this.option('verbosity') > 2 ) this.consoleNode.append(`<div class="alert alert-info" role="alert"> <span class="label label-default">SIGINT</span> <span class="label label-success">ACTIVE</span> <strong>${channel}</strong>  Block Control Listening <span><i class="fa fa-spinner fa-pulse"></i></span> </div>`);
    }

    if (this._prepare) this._prepare();
    this.refresh();
  },

  _setOptions: function( options ) {
      this._super( options );

      // when options change update screen
      this.refresh();
  },

  _constrain: function( value ) {
      if ( value > 100 ) {
          value = 100;
      }
      if ( value < 0 ) {
          value = 0;
      }
      return value;
  },

  _setOption: function( key, value ) {
    // fine grained option control
    if ( key === "value" ) {
      value = this._constrain( value );
    }
    this._super( key, value );
  },

  // refresh doubles as init.
  refresh: function() {
    // options have been updated
    // write to dom here.
    if (this.display) this.display();
  },

  destructors: function (f) {
    this.destructorArray.push(f);
  },

  _destroy: function () {
    this._superApply( arguments );
    this.destructorArray.forEach(destroy => {
      destroy.bind(this)();
    });
  },


});
